#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)

        # Leyendo línea a línea lo que nos envía el cliente
        mensaje = self.rfile.read().decode('utf-8')
        METODO = mensaje.split()[0]
        print("recibido, nos manda: ", mensaje)
        USER = mensaje.split()[2].split(':')[0]
        PUERTO = str(sys.argv[2])

        if METODO == "INVITE":
            try:
                Cuerpo = "v = 0\r\n" +\
                         "o = " + USER + "\r\n" + \
                         "s = sesionsara\r\n" + \
                         "t = 0\r\n" + \
                         "m = audio " + PUERTO + " RTP\r\n"
                Cabecera = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n" \
                            "\r\n\r\nSIP/2.0 200 OK\r\nContent-length:\r\n" \
                            + str(len(Cuerpo))
                Paquete = Cabecera + Cuerpo
                self.wfile.write(bytes(Paquete, 'utf-8'))
                print(Paquete)

            except IndexError:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
        elif METODO == "BYE":
            respuesta = (b"SIP/2.0 200 OK\r\n")
            self.wfile.write(respuesta)
        elif METODO == "ACK":
            ALEAT = random.randint(1, 11111)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, pad_flag=0, ext_flag=0, cc=0,
                                  marker=0, payload_type=14, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio, "127.0.0.1", 23032)
            print(ALEAT)
        else:
            respuesta = (b"SIP/2.0 405 Method Not Allowed\r\n")
            self.wfile.write(respuesta)


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        if len(sys.argv) != 4:
            sys.exit("Usage: python3 server.py IP PORT fichero_audio")
        IP = sys.argv[1]
        PUERTO = int(sys.argv[2])
        audio_file = sys.argv[3]
    except ValueError:
        sys.exit("Usage: python3 server.py IP PORT fichero_audio")

    serv = socketserver.UDPServer((IP, PUERTO), EchoHandler)
    print("Listening...")
    serv.serve_forever()
