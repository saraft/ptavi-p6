#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
# Cliente UDP simple.

# Dirección IP del servidor.
try:
    METODO = sys.argv[1]
    DIRECCION = sys.argv[2].split("@")
    IP = DIRECCION[1].split(":")[0]
    PUERTO = int(sys.argv[2].split(":")[1])
    USUARIO = sys.argv[2].split(":")[0]

except ValueError:
    sys.exit("Usage: python3 client.py METODO UA@IPUA:PORTsip")

# Contenido que vamos a enviar
LINE = METODO + ' sip: ' + USUARIO + ' SIP/2.0\r\n'

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, PUERTO))

    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print(data.decode('utf-8'))

    if METODO == 'INVITE':
        print("Enviando: " + LINE)
        newLine = "ACK" + " sip: " + USUARIO + ' SIP/2.0\r\n'
        my_socket.send(bytes(newLine, 'utf-8') + b'\r\n')
        print(newLine)
    if METODO == 'BYE':
        print("Enviando: " + LINE)
        print("Terminando socket...")

print("Fin.")
